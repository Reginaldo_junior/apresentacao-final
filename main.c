/*	
 *	Criado por: Reginaldo Pereira Junior - Aluno Unilins
 *
 *	Objetivo: 
 *		Exemplo usando TIMER/Couter, I/O e Sistemas de
 *		interrupção.
 *
 *		Tags: I/O, PWM, Delay EXTI0_IRQHandler. 
 *
 *
 * *///-------------------- Tipos de dados --------------------  
#define __IO volatile  //Habilita RW
#define uint32_t unsigned int
#define u32 unsigned int

#define uint8_t unsigned char
#define u8 unsigned char

#define int32_t int

#define uint16_t unsigned short
#define u16 unsigned short

#define STACKINT	0x20000000

#define EXTI0_IRQn 6

//--------------------  GPIOx Def --------------------
#define GPIOA ((GPIO_TypeDef * ) 0x40010800) /* GPIOx Addr Base */
#define GPIOB ((GPIO_TypeDef * ) 0x40010c00) 
#define GPIOC ((GPIO_TypeDef * ) 0x40011000) 
#define GPIOD ((GPIO_TypeDef * ) 0x40011400) 
#define GPIOE ((GPIO_TypeDef * ) 0x40011800) 
#define GPIOF ((GPIO_TypeDef * ) 0x40011c00) 
#define GPIOG ((GPIO_TypeDef * ) 0x40012000) 

//-------------------- TIMx Def -------------------- 
#define TIM1 ((TIM_TypeDef * ) 0x40012c00) /* TIMx Addr Base */
#define TIM2 ((TIM_TypeDef * ) 0x40000000) 
#define TIM3 ((TIM_TypeDef * ) 0x40000400) 
#define TIM4 ((TIM_TypeDef * ) 0x40000800) 
#define TIM5 ((TIM_TypeDef * ) 0x40000c00) 
#define TIM6 ((TIM_TypeDef * ) 0x40001000) 
#define TIM7 ((TIM_TypeDef * ) 0x40001400) 
#define TIM8 ((TIM_TypeDef * ) 0x40013400) 
#define TIM9 ((TIM_TypeDef * ) 0x40014C00) 
#define TIM10 ((TIM_TypeDef * ) 0x40015000) 
#define TIM11 ((TIM_TypeDef * ) 0x40015400) 
#define TIM12 ((TIM_TypeDef * ) 0x40001800) 
#define TIM13 ((TIM_TypeDef * ) 0x40001c00) 
#define TIM14 ((TIM_TypeDef * ) 0x40002000) 
#define FLASH (( FLASH_TypeDef * ) 0x40022000)

//-------------------- AFIO 
#define AFIO (( AFIO_TypeDef *) 0x40010000 )

//-------------------- EXTI 
#define EXTI (( EXTI_TypeDef *) 0x40010400 )

//-------------------- RCC Def -------------------- 
#define RCC (( RCC_TypeDef *) 0x40021000) /* RCC Addr Base */

/*-------------------- NVIC --------------------*/
#define NVIC_BASE (( __IO u32 *) 0xE000E100) 
#define NVIC            ((NVIC_type  *)  NVIC_BASE)


//-------------------- Area de funções ---------------  

typedef struct {
   uint32_t   ISER[8];     /* Address offset: 0x000 - 0x01C */
   uint32_t  RES0[24];     /* Address offset: 0x020 - 0x07C */
   uint32_t   ICER[8];     /* Address offset: 0x080 - 0x09C */
   uint32_t  RES1[24];     /* Address offset: 0x0A0 - 0x0FC */
   uint32_t   ISPR[8];     /* Address offset: 0x100 - 0x11C */
   uint32_t  RES2[24];     /* Address offset: 0x120 - 0x17C */
   uint32_t   ICPR[8];     /* Address offset: 0x180 - 0x19C */
   uint32_t  RES3[24];     /* Address offset: 0x1A0 - 0x1FC */
   uint32_t   IABR[8];     /* Address offset: 0x200 - 0x21C */
   uint32_t  RES4[56];     /* Address offset: 0x220 - 0x2FC */
   uint8_t   IPR[240];     /* Address offset: 0x300 - 0x3EC */
   uint32_t RES5[644];     /* Address offset: 0x3F0 - 0xEFC */
   uint32_t       STIR;    /* Address offset:         0xF00 */
} NVIC_type;


typedef struct{
	__IO u32 CRL ; 		// 0x00 MODO de operação Port configuration register low
	__IO u32 CRH ; 		// 0x04 Port configuration register high
	__IO u32 IDR ;		// 0x08 Port input data register
	__IO u32 ODR ;		// 0x0c Port output data register
	__IO u32 BSRR ;		// 0x10 Port bit set/reset register
	__IO u32 BRR ;		// 0x14 Port bit reset register
	__IO u32 LCKR ;		// 0x18 Port configuration lock register

} GPIO_TypeDef ;

typedef struct{
		
	__IO u32 EVCR;		// 0x00
	__IO u32 MAPR;		// 0x04
	__IO u16 EXTICR1; 	// 0X08
	__IO u16 RES1 ; 	// 
	__IO u16 EXTICR2; 	// 0x0c
	__IO u16 RES2 ; 	// 
	__IO u16 EXTICR3; 	// 0x10
	__IO u16 RES3 ; 	//
	__IO u16 EXTICR4; 	// 0x14
	__IO u16 RES4 ; 	// 
	__IO u32 reservado;	// 0x18
	__IO u32 MAPR2;		// 0x1c

} AFIO_TypeDef ;

typedef struct{

	__IO u32 CR1; 		// 0x00
	__IO u32 CR2;		// 0x04
	__IO u32 SMCR;		// 0x08
	__IO u32 DIER;		// 0x0c
	__IO u32 SR; 		// 0x10
	__IO u32 EGR; 		// 0x14
	__IO u32 CCMR1;		// 0x18 ->  input/output capture mode
	__IO u32 CCMR2;		// 0x1c ->  input/output capture mode
	__IO u32 CCER; 		// 0x20 <--- 
	__IO u32 CNT; 		// 0x24
	__IO u32 PSC;		// 0x28
	__IO u32 ARR;		// 0x2c
	__IO u32 RCR;		// 0x30
	__IO u32 CCR1; 		// 0x34
	__IO u32 CCR2;		// 0x38 
	__IO u32 CCR3;		// 0x3c
	__IO u32 CCR4;		// 0x40
	__IO u32 BDTR;		// 0x44 -> Descritivo na pagina 344 do RM0008
	__IO u32 DCR;		// 0x48
	__IO u32 DMAR;		// 0x4c

} TIM_TypeDef ;

typedef struct{
//		       		  Offset
	__IO u32 CR; 		// 0x00
	__IO u32 CFGR;		// 0x04 
	__IO u32 CIR;		// 0x08
	__IO u32 APB2RSTR;	// 0x0c
	__IO u32 APB1RSTR;	// 0x10
	__IO u32 AHBENR;	// 0x14
	__IO u32 APB2ENR;	// 0x18
	__IO u32 APB1ENR;	// 0x1c
	__IO u32 BDCR;		// 0x20
	__IO u32 CSR;		// 0x24
	__IO u32 AHBRSTR;	// 0x28
	__IO u32 CFGR2;		// 0x2c

} RCC_TypeDef;

typedef struct{
	
	__IO u32 GOTGCTL; 		// 0x00
   __IO u32 GOTGINT;		// 0x04
   __IO u32 GAHBCFG;		/* 0x08 <<- Responsavel por habilitar 
					  interrupções AHB ou USB */
   __IO u32 GUSBCFG;		// 0x0c
  	__IO u32 GRSTCTL;		// 0x10
  	__IO u32 GINTSTS;		// 0x14
  	__IO u32 GINTMSK;		// 0x18
  	__IO uint16_t GRXSTSR_1_H;	// 0x1c Host/Device Mode
  	__IO uint16_t GRXSTSR_1_D;	// 0x1c Host/Device Mode
  	__IO uint16_t GRXSTSR_2_H;	// 0x20 Host/Device Mode
  	__IO uint16_t GRXSTSRPR_2_D;	// 0x20 Host/Device Mode
   __IO u32 GRXFSIZ;		// 0x24
	__IO uint16_t  HNPTXFSIZ;	// 0x28 
	__IO uint16_t  DIEPTXF0;	// 0x28
	__IO u32 HNPTXSTS;		// 0x2c

} OTG_FS_TypeDef;

typedef struct {
	
	__IO u32 IMR ;		// 0x00
	__IO u32 EMR ;		// 0x04	
	__IO u32 RTSR ;		// 0x08	
	__IO u32 FTSR ;		// 0x0c	
	__IO u32 SWIER ;	// 0x10	
	__IO u32 PR ;		// 0x14	

} EXTI_TypeDef;

void SystemInit (void) {
  RCC->CR |= (uint32_t)0x00000001;
  RCC->CFGR &= (uint32_t)0xF8FF0000;
  RCC->CR &= (uint32_t)0xFEF6FFFF;
  RCC->CR &= (uint32_t)0xFFFBFFFF;
  RCC->CFGR &= (uint32_t)0xFF80FFFF;
  //RCC->CIR = 0x009F0000;
 }

void nvic_intEnable(u8 IRQn){ 
   NVIC->ISER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); }

void nvic_IntDisble(u8 IRQn){ 
   NVIC->ICER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); }

void set_system_clock_to_25Mhz (void){
	RCC->CR 	|= 1<<16 ;
	while (!( RCC->CR & (1<<17) ));	
	RCC->CFGR 	|= 1<<0;
}

//########################################################
//##############  Definições Globais ↓↓↓↓ ################
#define M 10000

#define Limit 4  /* Limite total de teclas de entrada*/
__IO u32 IRW=0000; /* Valor escaneado momentâneo */
__IO u32 arrayIR[3]={0,0,0}; /* Para controle de existencia*/
__IO u32 AcaoIR[Limit];  /* Array de sinais infra vermelho*/
__IO u8 SanitEXTI=0;

u8 Cont=0; /*Controla o indice de sinais IR escaneados*/

u16 Intens = 500;

void leituraIR();
void showWordIR( u32 Var );
void Delay ( __IO u32 T );
void Verif();
//########################################################
//########################################################


//%%%%%%%%%%%%%%% External interruptions %%%%%%%%%%%%%%

void EXTI0_IRQHandler (){ 
	/* Desabilita por que deve chamar uma vez	até a 
	 * palavra estar completa.*/
	EXTI->PR |=1<<0; /* Limpa o bit de "pendencia" do EXTI0*/
	nvic_IntDisble(EXTI0_IRQn); 

	if (SanitEXTI == 0 ){
			//GPIOB->ODR ^= (1<<11);/*SCK -> Sanit Check*/
		  leituraIR();
			  SanitEXTI = 1;
	}
	else 
			  SanitEXTI = 0;

Delay(10000*2);
	nvic_intEnable(EXTI0_IRQn ) ;
//GPIOB->ODR ^= (1<<11);/*SCK -> Sanit Check*/
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


void leituraIR(){ // FUNCIONAAAA
Delay(2);

	for (u8 i=0 ; i<32 ; i++){
		if ((GPIOB->IDR & ( 1<< 0 )) ) {
			Delay(2);
				if ((GPIOB->IDR & ( 1<< 0 )) ) {
				 Delay(3);
				if ((GPIOB->IDR & ( 1<< 0 )) ) {
				 Delay(3);
				if ((GPIOB->IDR & ( 1<< 0 )) ) {
				 Delay(3);
				  if ((GPIOB->IDR & ( 1<< 0 )) )
							 IRW &= ~(1 << (31-i));
				}}}
		}
		else 
	  IRW |= (1 << (31-i));
		Delay(16 * i*2 );
	}

// showWordIR(IRW);
 Verif();
}

void Verif(){
arrayIR[1] = IRW;
u8 t=0, FG=0;

	while (( t <= Cont) && ( FG !=0xF )) {
				if ( AcaoIR[t] == IRW ) {
					FG=0xF;
				}
				t++;
	}

	if ((( arrayIR[0] != 0) 
				&& (arrayIR[0] ==  arrayIR[1])) 
						&& ( Cont < Limit )
						&& (FG != 0xF ))  { 


		AcaoIR[Cont] = IRW ; /* Array de sinais infra vermelho*/
		Cont ++; // Controle indice de tecla
		showWordIR( arrayIR[Cont -1] );
	}

	else if ( Cont < Limit && (FG != 0xF ) )   
		arrayIR[0] = IRW;

	else if ( Cont != Limit  ) {
	 for ( t=0 ; t <10 ; t++){
			Delay(M);
			GPIOB->ODR ^=(0xf<<12);}
	}

}

void showWordIR( u32 Var ){	
//	 IRW=0b0101110011100010100101111000111UL;
u8 c=0;
	
//	for (u8 i=0 ; i < 4 ; i++){	
	while ( c < 8 ) {
			GPIOC->ODR ^= (1<<13);

		if ( (Var & (1 << (c + 23)) ))
			GPIOB->ODR &= ~(1<<12);
		else 
			GPIOB->ODR |= (1<<12);


		if ( (Var & (1<<(c + 15))) )
			GPIOB->ODR &= ~(1<<13);
		else 
			GPIOB->ODR |= (1<<13);


		if ( (Var & (1<<(c + 7))) )
			GPIOB->ODR &= ~(1<<14);
		else 
			GPIOB->ODR |= (1<<14);


		if ( (Var & (1<< c)) )
			GPIOB->ODR &= ~(1<<15);
		else 
			GPIOB->ODR |= (1<<15);


		Delay(10000*2);
			GPIOB->ODR |=(0xF<<12);
			c++;
	}

}


void enable_TIM3_delay (void) {
	RCC->APB1ENR |= 1<<1 ;
	TIM3->CR1 = 0x0000;
	//TIM3->ARR = 21699; 
	TIM3->ARR = 250-1; // 10µs
	// para obter o período de 868µs 
/*https://www.vishay.com/docs/80071/dataform.pdf PAG 2
 * https://controllerstech.com/ir-remote-with-stm32/ 
 * Per=(1+ARR)/Clock_system */ 
	TIM3->CR1 |= 1<<0;
}

void configEXTI(){
	u16 pb0 = (1u<<0); // Pino B0 ref
	GPIOB->CRL	    |= 0x04u << 0; /* Pino B0 → inputFloat */

	/*U = unsigned ; UL = Unsigned Long */
	RCC->APB2ENR       |=1u<<0; /* AFIO */
	AFIO->EXTICR1	&= ~0xffffU; /* clean */
	AFIO->EXTICR1	|= 0x1u<<0; /* tabela_2 referência a GPIOB */

/* Habilita a requisição de interrupção EXTI1 */
/* Seleção de borda
			 * RISING 	↑  RTSR 1 FTSR 0
			 * FALLING	 ↓ RTSR 0 FTSR 1
			 * BOTH		↑↓ RTSR 1 FTSR 1
			 * */
/* Habilita a mascara de interrupções "IMR" e o evento de
 * interrupção "EMR"*/
	EXTI->RTSR |= pb0;
	EXTI->FTSR &= ~pb0;
	EXTI->IMR  |= pb0;
	EXTI->EMR  |= pb0;

	nvic_intEnable( EXTI0_IRQn );
   NVIC->IPR[ EXTI0_IRQn ] = 0x0; // Priori MAX

}

void Delay ( __IO u32 T ) { // T=1 ;  868µs -> T=1 ~ 526µs
  for ( T ; T > 0 ; T-- ){
   TIM3->EGR |= ( 1 << 0 );
   while ( TIM3->CNT < 5 ) ; /* 5 * 10-- Configurar para IR */
  }
}

#define psc 100
#define arr 999
void PWM_Configure(){
	/* Configurado com o exemplo do datasheet pag "387 / 1136" */

	RCC->APB2ENR |= 0b101 << 0 ; /*Habilita o AFIO GPIOA */
	RCC->APB1ENR |= 1 << 0; /*Habilita o TIMER 2 - CH na GPIO_A */

	GPIOA->CRL |= (0x0B << 0) ; /* AF O_PP pino PA0 */
	AFIO->MAPR &= ~( 0b11 << 8); /* [bit 26:24] SWJ_CFG [nota_2]
					  [bit 8] TIM3_REMAP[1:0]: TIM2 remapping [nota_3]*/ 

   TIM2->CCMR1 |= 0b110 << 4; /*Bits 6:4 OC1M: Output compare 1 mode
											 110: PWM mode 1 */
   TIM2->CCMR1 |= 1UL << 3; /* Bit 3 OC1PE: Output compare 1 preload enable**/

   TIM2->PSC = psc; /* 25/250 - 1 => 0.1KHz  */
   TIM2->ARR = arr; /* PWM period = (999 + 1) * 100KHz = 0.01 */

	TIM2->CR1 &= ~(1<<4);
   TIM2->CR1 |= 1<<7;/*Bit 7 ARPE: Auto-reload preload enable */
   TIM2->CCMR1 |= 1UL << 3; /* Bit 3 OC1PE: Output compare 1 preload enable - ---- CONSEGUIIIIIII **/
	TIM2->EGR |=1<<0;

   TIM2->CCER |= 1<<0; /*Bit 0 CC1E: Capture/Compare 1 output enable*/
   TIM2->CCER &= ~(1<<1); /*Bit 1 CC1P: Capture/Compare 1 output polarity
                            0: OC1 active high. */

   TIM2->CR1 |= 1<<0; //Habilita o clock
   TIM2->CCR1 = 00; 


}

void pinOut_configure(){
	RCC->APB2ENR  |= 1<<3 ; /*GPIOB  */
	GPIOC->CRH	|= ( 0x03 << 20 ); // Out_PP C13

	GPIOB->CRH  |= ( 0x03 << 16 ); // Out_PP B12
	GPIOB->CRH  |= ( 0x03 << 20 ); // Out_PP B13
	GPIOB->CRH  |= ( 0x03 << 24 ); // Out_PP B14
	GPIOB->CRH  |= ( 0x03 << 28 ); // Out_PP B15
											 //
	GPIOB->CRH  |= ( 0x03 << 12 ); // Out_PP B11
											 //
	GPIOB->CRL	|= 0x04 << 4 ; /*pino B1 pull up/donw*/

	GPIOB->ODR |=(0b11111<<11);
}

void PWM_Control(){
	
	if ( AcaoIR[1] == IRW  ){
		Intens += 0xC0;
		GPIOB->ODR ^= (1<<12);
	}
	else if( AcaoIR[2] == IRW && Intens > 0){
		/*Para não gerar numero negativo*/
		Intens -= 0xC0;
		GPIOB->ODR ^= (1<<13);
	}	
	else if( AcaoIR[3] == IRW ){
		GPIOB->ODR ^= (1<<14);
			for (u16 i=0 ; i<=0x0fff ; i++){ // 1 → 0
				// ↑fisicamente  0=Max 0x0FFF = Min
				Delay (100);
				TIM2->CCR1 = i;
			}
			for (u16 i=0x0fff; i>10 ; i--){ // 0 → 1
				Delay (100);
				TIM2->CCR1 = i;
			} 

	}
	else if( AcaoIR[0] == IRW && (Cont == Limit) ){
		Cont=0; // Reseta o keyboard
		GPIOB->ODR &=~(0b11111<<11);
		Delay(M*10);
		GPIOB->ODR |=(0b11111<<11);
		Intens = 500;
		for (u8 K=1; K <Limit ; K++)	
			AcaoIR[K]=0xFFFF;

	}
	TIM2->CCR1 = Intens;
	IRW =0x777;

}

int32_t main (void) {
	RCC->APB2ENR |= ( 1<<4 ) ; //Habilita a GPIOA,C
	GPIOC->CRH	|= ( 0x03 << 20 ); // Out_PP C13
											 //
	enable_TIM3_delay(); /* TIM3 */
	PWM_Configure();

	pinOut_configure();
	configEXTI(); // -> EXTI1

	 while (1){
		 Delay(M * 2);
       GPIOC->ODR ^= 1<<13;
		 PWM_Control();


    }
	 return 0;


}

/* Definições de bits "CFG" e "Mode" da configuração das GPIOs.
 *		   Pin_Hardw	
 *   CFG     MODE    H	 L   
 * |31|30|  |29|28| 15 ; 7		
 * |27|26|  |25|24| 14 ; 6	
 * |23|22|  |21|20| 13 ; 5
 * |19|18|  |17|16| 12 ; 4
 * |15|14|  |13|12| 11 ; 3
 * |11|10|  | 9| 8| 10 ; 2
 * | 7| 6|  | 5| 4|  9 ; 1
 * | 3| 2|  | 1| 0|  8 ; 0
 *
 *
 * __________________________________________
 * HexaCode -> 50MHz
 * 0x03 | 0| 0|  |01..11| <- Out Push-pull
 * 0x07 | 0| 1|  |01..11| <- Out Open-drain
 * 0x0B | 1| 0|  |01..11| <- Alt Out *pull
 * 0x0B | 1| 0|  |01..11| <- Alt Out *drain
 * __________________________________________
 * 0x00 | 0| 0|  |  00  | -< Input Analogig
 * 0x04 | 0| 1|  |  00  | -< Input Floating  
 * 0x08 | 1| 0|  |  00  | -< Input Pull-{down,up}
 * __________________________________________
 * */

/* tabela_2 -> Configuração do Registrador AFIO->EXTICRx
 *     EXTI[0..15] 3    2    1    0 
 *
 *     Shift     <<12  <<8  <<4  <<0 	
 *	              xxxx xxxx xxxx xxxx 
 * GPIO[a..g] pin  3    2    1    0   	AFIO_EXTI_CR_1
 * GPIO[a..g] pin  7    6    5    4  	AFIO_EXTI_CR_2 
 * GPIO[a..g] pin  11   10   9    8  	AFIO_EXTI_CR_3 
 * GPIO[a..g] pin  15   14   13   12  	AFIO_EXTI_CR_4 
 *
 *Hex  xxxx
 *0x00 0000: PA[x] pin 
 *0x01 0001: PB[x] pin 
 *0x02 0010: PC[x] pin 
 *0x03 0011: PD[x] pin 
 *0x04 0100: PE[x] pin 
 *0x05 0101: PF[x] pin 
 *0x06 0110: PG[x] pin
 *
 * */

/* link_1 https://developer.arm.com/documentation/dui0662/b/Cortex-M0--Peripherals/Nested-Vectored-Interrupt-Controller/Interrupt-Set-Enable-Register
 *
 *
 *
 * https://github.com/98zam98/arm_nvic_driver/blob/main/hardware_arm_nvic_driver.h
 */

/*
 * Neste código, fora resolvido o problema com a interrupção
 * acredito que definitivamente usando o projeto:
 * "stm32f1-bare-metal/ext_int/ext_int.c"
 *		Usando uma estrutura com as definições de  vetores.
 *		Fora usado também, o manual do programador:
 * 
 * dm00046982-stm32-cortex-m4-mcus-and-mpus-programming-manual-stmicroelectronics.pdf 
 */



/* TRASH AREA
 *
 *
  while ( (!(GPIOB->IDR & (1<<0 ))) || c < 12 ){
				  c++;
				  Delay(5);
		}		  
		
		while ( (GPIOB->IDR & (1<<0  )) || c < 12 ){
				  c++;
				  Delay(5);
		}

	1 - > true  1==1
	0 - > false 1==0
 *
 *
 *
 * */

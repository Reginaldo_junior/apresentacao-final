
TARGET = OutCode


SRCS = main.c startup_stm32f103xb.s
LINKER_SCRIPT = STM32F103C8Tx_FLASH.ld

OBJS =  $(addsuffix .o, $(basename $(SRCS)))
INCLUDES = -I.


CFLAGS += -mcpu=cortex-m3 -mthumb # Processor setup
CFLAGS += -O0  # Optimization is off
CFLAGS += -g3  # Generate debug information
CFLAGS += -gstabs+
CFLAGS += -fno-common -Wall 
#CFLAGS += -Werror # variaveis inuteis
CFLAGS += -ffunction-sections -fdata-sections -Wl,--gc-sections

LDFLAGS += -march=armv7-m
LDFLAGS += -nostartfiles
LDFLAGS += --specs=nosys.specs
LDFLAGS += -T$(LINKER_SCRIPT)

CROSS_COMPILE = arm-none-eabi-
CC = $(CROSS_COMPILE)gcc
LD = $(CROSS_COMPILE)ld
OBJDUMP = $(CROSS_COMPILE)objdump
OBJCOPY = $(CROSS_COMPILE)objcopy
SIZE = $(CROSS_COMPILE)size

all: clean $(SRCS) build size
	echo "Successfully finished..."

build: $(TARGET).elf $(TARGET).hex $(TARGET).bin $(TARGET).lst

$(TARGET).elf: $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $@

%.o: %.c
	echo "Building" $<
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

%.o: %.s
	echo "Building" $<
	$(CC) $(CFLAGS) -c $< -o $@

%.hex: %.elf
	$(OBJCOPY) -O ihex $< $@

%.bin: %.elf
	$(OBJCOPY) -O binary $< $@

%.lst: %.elf
	$(OBJDUMP) -x -S $(TARGET).elf > $@

size: $(TARGET).elf
	$(SIZE) $(TARGET).elf

#load
l:
	st-flash write $(TARGET).bin 0x8000000

# clean
c:
	@echo "Cleaning..."
	@rm -vf $(TARGET).elf
	@rm -vf $(TARGET).bin
	@rm -vf $(TARGET).map
	@rm -vf $(TARGET).hex
	@rm -vf $(TARGET).lst
	@rm -vf $(TARGET).o
	@rm -vf *.o

.PHONY: all build size clean burn

